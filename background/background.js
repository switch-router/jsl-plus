const DATA_CB_REGEXP = /data_cb.[0-9a-fA-F]{8}.js$/
const REDIR_DATA_CB_JS = 'data_cb.a99e8b02.js'

var extension_enable = false
var redir_success = false

chrome.runtime.onInstalled.addListener(() => {
    chrome.webRequest.onBeforeRequest.addListener(
        function (details) {
            if (DATA_CB_REGEXP.test(details.url)) {
                if (extension_enable === true) {
                    if (redir_success === false) {
                        redir_success = true
                        chrome.browserAction.setIcon({ path: "/19-blue.png"})
                    }
               //   return { redirectUrl: chrome.extension.getURL(REDIR_DATA_CB_JS) };
                    return { redirectUrl: "https://gitee.com/switch-router/jsl-plus-js/releases/download/stable/data_cb.redirect.js"}
                }
            }
        },
        { 
            types:["script"],
            urls: ["*://*/*.js"]
        },
        ["blocking"]
    );
});

chrome.browserAction.onClicked.addListener(function(tab) {
    if (extension_enable === false) {
        console.log("Enable jsl plus")
        extension_enable = true
        chrome.browserAction.setIcon({ path: "/19-yellow.png" });
    } else {
        console.log("Disable jsl plus")
        redir_success = false
        extension_enable = false
        chrome.browserAction.setIcon({ path: "/19-gray.png" });
    }
});

